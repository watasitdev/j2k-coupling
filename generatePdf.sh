#!/bin/bash

rm -rf wiki j2k_documents
git clone https://gitlab.irstea.fr/watasitdev/j2k-coupling.wiki.git wiki
cd wiki
sed -i 's/\[\[_TOC_\]\]//g' *.md
for i in *.md; do
    pandoc -o "${i/.md/.pdf}" "$i"
done
cd ..
mkdir j2k_documents
mv wiki/*.pdf ./j2k_documents/
