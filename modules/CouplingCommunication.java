/*
 * CouplingCommunication.java
 * The most powerfull J2000 module!
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 */
package cowattools;

import jams.data.*;
import jams.model.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Calendar;
import java.util.List;
import java.util.Arrays;

import java.util.concurrent.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import org.json.JSONObject;
import org.json.JSONArray;
import jams.JAMSException;

/**
 *
 * @author c6gohe2
 */
@JAMSComponentDescription(title = "CouplingCommunication",
        author = "Julien Veyssier and Bruno Bonté",
        description = "Declares a communication object and waits for an external entity to order a step with a HTTP GET request. Place this module on top of time loop.",
        version = "0.1",
        date = "2020-01-23")

public class CouplingCommunication extends JAMSComponent {
    
    // IRRIGATION
    private static Map<String, Double> irrigAspersion;
    private static Map<String, Double> irrigDrip;
    private static Map<String, Double> irrigSurface;
    
    // INFILTRATION
    private static Map<String, Double> infiltration;
    
    // what comes in and out of reachs
    private static Map<String, Double> outOfReach;
    private static Map<String, Double> backToReach;
    
    // modification modules values
    private static Map<String, Map<String, Double>> modifModuleTables;
    
    private static CouplingCommunication singleton;

    private int mi = 0;
    private ComServer comServer;

    // released when a step can be done
    private Semaphore sOkStep;
    // released when the server process another connection
    private Semaphore sAcceptConnection;

    @JAMSVarDescription(access = JAMSVarDescription.AccessType.READ,
            description = "Current time")
    public Attribute.Calendar time;
    
    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READ,
            description = "The reach collection"
    )
    public Attribute.EntityCollection reachs;
    
    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READ,
            description = "HRUs list"
    )
    public Attribute.EntityCollection hrus;

    /* This attribute is on readwrite only because it is the
    *  first time it appears in the timeloop but it will only
    *  be read in this module.
    */
    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READWRITE,
            description = "catchment simRunoff"
    )
    public Attribute.Double catchmentSimRunoff;
    
        @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READ,
            description = "Tell if we run in debug mode (no timeout)"
    )
    public Attribute.Boolean debugMode;
    
    public void init() {
        singleton = this;
        //double t = 1;
        log("CouplingCommunication init");

        sOkStep = new Semaphore(0);
        sAcceptConnection = new Semaphore(1);

        startServer();
        
        irrigAspersion = new HashMap<>();
        irrigDrip = new HashMap<>();
        irrigSurface = new HashMap<>();
        
        infiltration = new HashMap<>();
        
        outOfReach = new HashMap<>();
        backToReach = new HashMap<>();
        
        modifModuleTables = new HashMap<>();
    }
    
    // can be used from a static context or another module like that:
    // singleton.getStep() OR CouplingCommunication.singleton.getStep()
    public int getStep() {
        return mi;
    }
    
    public void resetModificationTables() {
        irrigAspersion.clear();
        irrigDrip.clear();
        irrigSurface.clear();
        infiltration.clear();
        outOfReach.clear();
        backToReach.clear();
        
        modifModuleTables.clear();
    }
    
    public static Double getTableValue(String tableName, String key) {
        // get the table
        Map<String, Double> table = null;
        if (tableName.equals("aspersion")) {
            table = irrigAspersion;
        }
        else if (tableName.equals("drip")) {
            table = irrigDrip;
        }
        else if (tableName.equals("surface")) {
            table = irrigSurface;
        }
        else if (tableName.equals("infiltration")) {
            table = infiltration;
        }
        else if (tableName.equals("reachout")) {
            table = outOfReach;
        }
        else if (tableName.equals("reachin")) {
            table = backToReach;
        } else {
            table = modifModuleTables.getOrDefault(tableName, null);
        }
        
        if (table == null) {
            return null;
        } else {
            return table.getOrDefault(key, null);
        }
    }
    
    public static void setTableValues(String tableName, JSONObject values) {
        // get the table to be filled
        Map<String, Double> tableToEdit = null;
        if (tableName.equals("aspersion")) {
            tableToEdit = irrigAspersion;
        }
        else if (tableName.equals("drip")) {
            tableToEdit = irrigDrip;
        }
        else if (tableName.equals("surface")) {
            tableToEdit = irrigSurface;
        }
        else if (tableName.equals("infiltration")) {
            tableToEdit = infiltration;
        }
        else if (tableName.equals("reachout")) {
            tableToEdit = outOfReach;
        }
        else if (tableName.equals("reachin")) {
            tableToEdit = backToReach;
        } else {
            tableToEdit = modifModuleTables.getOrDefault(tableName, null);
            if (tableToEdit == null) {
                tableToEdit = new HashMap<>();
            }
            modifModuleTables.put(tableName, tableToEdit);
        }

        tableToEdit.clear();
        // put values
        Iterator<String> keys = values.keys();
        String key;
        Double val;
        while (keys.hasNext()) {
            key = keys.next();
            val = values.getDouble(key);
            tableToEdit.put(key, val);
            System.out.println("Coupling module set "+tableName+" of KEY "+key+" TO "+val);
        }
    }
    
    public String getReachInfoDict(Double selectedId) {
        Iterator<Attribute.Entity> reachIterator = reachs.getEntities().iterator();
        Attribute.Entity reach;
        Double actRD1;
        Double reachId;
        String sReachId;
        String result = "{\n";
        while (reachIterator.hasNext()) {
            reach = reachIterator.next();
            reachId = reach.getDouble("ID");
            if (selectedId == null || selectedId == reachId) {
                actRD1 = reach.getDouble("actRD1");
                sReachId = String.valueOf(reachId.intValue());
                result += "\""+sReachId+"\": {\"actRD1\": "+actRD1+"},\n";
            }
        }
        result = result.replaceAll(",$", "");
        result += "}\n";
        return result;
    }

    public String getReachInfoList(Double selectedId) {
        Iterator<Attribute.Entity> reachIterator = reachs.getEntities().iterator();
        Attribute.Entity reach;
        Double actRD1;
        Double reachId;
        String sReachId;
        String result = "[\n";
        while (reachIterator.hasNext()) {
            reach = reachIterator.next();
            reachId = reach.getDouble("ID");
            if (selectedId == null || selectedId == reachId) {
                actRD1 = reach.getDouble("actRD1");
                sReachId = String.valueOf(reachId.intValue());
                result += "{\"ID\": \""+sReachId+"\", \"actRD1\": "+actRD1+"},\n";
            }
        }
        result = result.replaceAll(",$", "");
        result += "]\n";
        return result;
    }
    
    public String getHruInfoList(Double selectedId) {
        Iterator<Attribute.Entity> hruIterator = hrus.getEntities().iterator();
        Attribute.Entity hru;
        Double netRain;
        Double hruId;
        String sHruId;
        String result = "[\n";
        while (hruIterator.hasNext()) {
            hru = hruIterator.next();
            hruId = hru.getDouble("ID");
            if (selectedId == null || selectedId == hruId) {
                netRain = hru.getDouble("netrain");
                sHruId = String.valueOf(hruId.intValue());
                result += "{\"ID\": \""+sHruId+"\", \"netRain\": "+netRain+"},\n";
            }
        }
        result = result.replaceAll(",$", "");
        result += "]\n";
        return result;
    }

    public String getHruInfoDict(Double selectedId) {
        Iterator<Attribute.Entity> hruIterator = hrus.getEntities().iterator();
        Attribute.Entity hru;
        Double netRain;
        Double hruId;
        String sHruId;
        String result = "{\n";
        while (hruIterator.hasNext()) {
            hru = hruIterator.next();
            hruId = hru.getDouble("ID");
            if (selectedId == null || selectedId == hruId) {
                netRain = hru.getDouble("netrain");
                sHruId = String.valueOf(hruId.intValue());
                result += "\""+sHruId+"\": {\"netRain\": "+netRain+"},\n";
            }
        }
        result = result.replaceAll(",$", "");
        result += "}\n";
        return result;
    }

    public String getAttribAllHruList(List<String> attributes, List<Long> requestedIds) {
        Iterator<Attribute.Entity> hruIterator = hrus.getEntities().iterator();
        List<Attribute.Entity> requestedEntities = new ArrayList(); // used only if some entities are requested
        long id;
        if (!requestedIds.isEmpty()){
            Iterator<Long> idsIterator = requestedIds.iterator();
            while(idsIterator.hasNext()) {
                Attribute.Entity selectedEntity;
                id = idsIterator.next();
                selectedEntity = hrus.getEntity(id);
                try{
                    if(selectedEntity == null){
                        throw new JAMSException("Hru nb "+id+" not found in Hrus");
                    }
                } catch(Exception e) {
                    return("Hru nb "+id+" not found in Hrus");
                }
                requestedEntities.add(selectedEntity);
            }
            hruIterator = requestedEntities.iterator();
        }
        Attribute.Entity hru;
        Double value;
        Double hruId;
        String sHruId;
        String result = "[\n";
        while (hruIterator.hasNext()) {
            hru = hruIterator.next();
            hruId = hru.getDouble("ID");
            sHruId = String.valueOf(hruId.intValue());
            result += "{\"ID\": \"" + sHruId + "\"";
            Iterator<String> attNameIterator = attributes.iterator();
            while (attNameIterator.hasNext()) {
                String attribute = attNameIterator.next();
                try {
                    value = hru.getDouble(attribute);
                } catch (Exception e) {
                    return "Attribute "+attribute+" not found for hrus";
                }
                
                result += ", \""+attribute+"\": " + value;
            }
            result += "},\n";
        }
        result = result.replaceAll(",$", "");
        result += "]\n";
        return result;
    }

    public String getAttribAllReachList(List<String> attributes, List<Long> requestedIds) {
        Iterator<Attribute.Entity> reachIterator = reachs.getEntities().iterator();
        List<Attribute.Entity> requestedEntities = new ArrayList(); // used only if some entities are requested
        long id;
        if (!requestedIds.isEmpty()){
            Iterator<Long> idsIterator = requestedIds.iterator();
            while(idsIterator.hasNext()) {
               Attribute.Entity selectedEntity;
                id = idsIterator.next();
                selectedEntity = reachs.getEntity(id);
                try{
                    if(selectedEntity == null){
                        throw new JAMSException("Reach nb "+id+" not found in reachs");
                    }
                } catch(Exception e) {
                    return("Reach nb "+id+" not found in reachs");
                }
                requestedEntities.add(selectedEntity);
            }
            reachIterator = requestedEntities.iterator();
        }
        Attribute.Entity reach;
        Double value;
        Double reachId;
        String sReachId;
        String result = "[\n";
        while (reachIterator.hasNext()) {
            reach = reachIterator.next();
            reachId = reach.getDouble("ID");
            sReachId = String.valueOf(reachId.intValue());
            result += "{\"ID\": \"" + sReachId + "\"";
            Iterator<String> attNameIterator = attributes.iterator();
            while (attNameIterator.hasNext()) {
                String attribute = attNameIterator.next();
                try {
                    value = reach.getDouble(attribute);
                } catch (Exception e) {
                    return "Attribute "+attribute+" not found for reachs";
                }
                result +=  ", \""+attribute+"\": " + value;
            }
        result += "},\n";
        }
        result = result.replaceAll(",$", "");
        result += "]\n";
        return result;
    }
    
    public String getAttribAllHruSumed(List<String> attributes) {
        Attribute.Entity hru;
        Double value;
        String result = "{\n";
        Iterator<String> attNameIterator = attributes.iterator();
        while (attNameIterator.hasNext()) {
            Iterator<Attribute.Entity> hruIterator = hrus.getEntities().iterator();
            String attribute = attNameIterator.next();
            value = 0.0;
            while (hruIterator.hasNext()) {
                hru = hruIterator.next();
                try {
                    value += hru.getDouble(attribute);
                } catch (Exception e) {
                    return "Attribute "+attribute+" not found for hrus";
                }
            }
                result += " \""+ attribute+ "\": " + value + ",";
        }
        result = result.replaceAll(",$", "");
        result += "\n}";
        return result;
    }

    public String getAttribAllReachSumed(List<String> attributes) {       
        Attribute.Entity reach;
        Double value;
        String result = "{\n";
        Iterator<String> attNameIterator = attributes.iterator();
        while (attNameIterator.hasNext()) {
            Iterator<Attribute.Entity> reachIterator = reachs.getEntities().iterator();
            String attribute = attNameIterator.next();
            value = 0.0;
            while (reachIterator.hasNext()) {
                reach = reachIterator.next();
                try {
                    value += reach.getDouble(attribute);
                } catch (Exception e) {
                    return "Attribute "+attribute+" not found for reachs";
                }
            }
                result += " \""+ attribute+ "\": " + value + ",";
        }
        result = result.replaceAll(",$", "");
        result += "\n}";
        return result;
    }
    
        public String getCatchmentRunoff() {
        Double value;
        try {
        value = catchmentSimRunoff.getValue();
        }catch (Exception e) {
                return "Catchment simRunoff not found";
            }
        //String result = "{\"" + "catchmentRunOff" + "\": " + value + "}\n";
        return String.valueOf(value);
    }
        
    public String getCatchmentHruStorage() {
        // TODO: to add an argument to make it generic
        // (List<String> attributesList) not done yet
        Double value;
        Iterator<Attribute.Entity> hruIterator = hrus.getEntities().iterator();
        Attribute.Entity hru;
        value = 0.0;
        while (hruIterator.hasNext()) {
            hru = hruIterator.next();
            try {
                value += hru.getDouble("actMPS");
                value += hru.getDouble("actLPS");
                value += hru.getDouble("actDPS");
                value += hru.getDouble("TotSWE");
                value += hru.getDouble("storedInterceptedWater");
                value += hru.getDouble("actRG1");
                value += hru.getDouble("actRG2");
            } catch (Exception e) {
                return "Some attributes not found for hrus";
            }
        }
        return String.valueOf(value);
    }

    public String getCatchmentReachStorage() {
        // TODO: to add an argument to make it generic
        // (List<String> attributesList) not done yet
        Double value;
        Iterator<Attribute.Entity> reachIterator = reachs.getEntities().iterator();
        Attribute.Entity reach;
        value = 0.0;
        while (reachIterator.hasNext()) {
            reach = reachIterator.next();
            try {
                value += reach.getDouble("actRD1");
                value += reach.getDouble("actRD2");
                value += reach.getDouble("actRG1");
                value += reach.getDouble("actRG2");
            } catch (Exception e) {
                return "Some attributes not found for reachs";
            }
        }
        return String.valueOf(value);
    }
    
    public String getHruBilan() {
        Iterator<Attribute.Entity> hruIterator = hrus.getEntities().iterator();
        Attribute.Entity hru;
        Double value;
        Double hruId;
        String sHruId;

        Double ETR_etact;
        Double P_precip;

        Double Pbasin = 0.0;
        Double ETPbasin = 0.0;
        while (hruIterator.hasNext()) {
            hru = hruIterator.next();
            hruId = hru.getDouble("ID");
            sHruId = String.valueOf(hruId.intValue());

            try {
                ETR_etact = hru.getDouble("etact");
                // convert precip from mm to litres
                P_precip = hru.getDouble("precip") * hru.getDouble("area");
            } catch (Exception e) {
                return "Attributes not found for hrus";
            }

            Pbasin += P_precip;
            ETPbasin += ETR_etact;
        }

        Iterator<Attribute.Entity> reachIterator = reachs.getEntities().iterator();
        Attribute.Entity reach;
        Double runoff = 0.0;
        Double reachId;
        String sReachId;
        while (reachIterator.hasNext()) {
            reach = reachIterator.next();
            reachId = reach.getDouble("ID");
            // in case we just want the outlet reach
            //if (reachId == 25401) {
                try {
                    runoff += reach.getDouble("Runoff") / (1000*24*3600);
                } catch (Exception e) {
                    return "Attribute Runoff not found for reach 25401";
                }
            //}
        }

        Pbasin = Pbasin / (1000*24*3600);
        ETPbasin = ETPbasin / (1000*24*3600);
        System.out.println("BILAN : PB " + Pbasin + " - runoff " + runoff + " - ETP " + ETPbasin);
        Double bilan = Pbasin - ETPbasin - runoff;
        System.out.println("HERE IS THE BILAN : " + bilan);

        return String.valueOf(bilan);
    }

    public void run() {
        if (comServer.isRunning()) {
            //if (mi != 0) getHruBilan();
            log("Resetting modification values tables");
            resetModificationTables();
            log("CouplingCommunication waiting permission to launch run " + mi);
            try {
                sOkStep.acquire();
            } catch (InterruptedException exc) {
                System.out.println(exc);
            }

            /*System.out.println("CouplingCommunication !!DOING!! 2sec run " + i);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException exc) {
                System.out.println(exc);
            }*/
        }

        //log("CouplingCommunication **FINISHED** run "+i);
        mi++;

        if (comServer.isRunning()) {
            // tell the server it's ok for another step, we finished this one
            sAcceptConnection.release();
        }
    }

    public void cleanup() throws JAMSEntity.NoSuchAttributeException {
        log("STOPPING THREAD at simulation end");
        comServer.stopListening();
        comServer.stopTimeout();
        comServer.stop();
    }

    public void log(String msg) {
        System.out.println(msg);
        getModel().getRuntime().sendInfoMsg(msg);
    }

    public void logError(String msg) {
        System.err.println(msg);
        getModel().getRuntime().sendErrorMsg(msg);
    }

    private void startServer() {
        comServer = new ComServer(9999, "CouplingCommunicationThread");
        comServer.start();
    }

    private class TimeoutProcess extends Thread {

        private int nbSec;
        private ComServer server;

        public TimeoutProcess(int nbSec, ComServer server) {
            this.nbSec = nbSec;
            this.server = server;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(nbSec * 1000);
            } catch (InterruptedException exc) {
                System.out.println(exc);
            }
            log("timeout reached, KILLING server");
            // kill socket
            server.stopListening();
            log("after stop listening");
            //server.setRunning(false);
            //sOkStep.release();
            // stop the thread
            server.stop();
            getModel().getRuntime().sendHalt("Stop model because timeout reached");
        }
    }

    private class ComServer extends Thread {

        String threadName;
        int port;
        TimeoutProcess timeoutProcess = null;
        ServerSocket ss;
        int nbStepUntilNextCommand = 1;
        boolean running = true;

        public ComServer(int port, String threadName) {
            super(threadName);
            this.threadName = threadName;
            this.port = port;
            try {
                this.ss = new ServerSocket(port);
            } catch (Exception e) {
                System.err.println(e);
            }
        }

        public boolean isRunning() {
            return this.running;
        }

        public void setRunning(boolean running) {
            this.running = running;
        }

        public void stopListening() {
            log("in stoplistening");
            try {
                ss.close();
            } catch (Exception e) {
                log("Exception while closing socket : "+e.toString());
            }
        }

        public void startTimeout(int nbSec) {
            log("launching timeout of " + nbSec + " seconds");
            timeoutProcess = new TimeoutProcess(nbSec, this);
            timeoutProcess.start();
        }

        public void stopTimeout() {
            if (timeoutProcess != null) {
                timeoutProcess.stop();
            }
            timeoutProcess = null;
        }

        @Override
        public void run() {

            try {
                for (;;) {
                    log("server starting LISTENING on port " + port);

                    // Wait for a client to connect.
                    Socket client = ss.accept();
                    log("connection received");
                    stopTimeout();

                    // wait for component to tell us it has finished a step to process
                    // further resquest
                    sAcceptConnection.acquire(nbStepUntilNextCommand);
                    nbStepUntilNextCommand = 1;
                    log("connection being processed");

                    // Get input and output streams to talk to the client
                    BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                    PrintWriter out = new PrintWriter(client.getOutputStream());

                    // Start sending our reply, using the HTTP 1.1 protocol
                    out.print("HTTP/1.1 200 \r\n"); // Version & status code
                    out.print("Content-Type: text/plain\r\n"); // The type of data
                    out.print("Connection: close\r\n"); // Will close stream
                    out.print("\r\n"); // End of headers

                    // read the HTTP request header from the client
                    String line;
                    while ((line = in.readLine()) != null) {
                        if (line.length() == 0) {
                            break;
                        }
                        //out.print("LIIIINE " + line + "\r\n");
                    }

                    // read the post payload data
                    StringBuilder payloadBuilder = new StringBuilder();
                    while (in.ready()) {
                        payloadBuilder.append((char) in.read());
                    }
                    String payload = payloadBuilder.toString();
                    log("Payload data is: " + payload);

                    // process the payload
                    boolean commandIsGet = false;
                    JSONObject jsonPayload = new JSONObject(payload);
                    if (jsonPayload.has("command") && !jsonPayload.isNull("command")) {
                        String command = jsonPayload.getString("command");
                        if (command.equals("free")) {
                            out.print("Model is now free to run, server is down\n");
                            out.close();
                            in.close();
                            client.close();
                            this.running = false;
                            stopListening();
                            sOkStep.release();
                            break;
                        } else if (command.equals("stop")) {
                            out.print("model is now stopped and server is down\n");
                            out.close();
                            in.close();
                            client.close();
                            stopListening();
                            getModel().getRuntime().sendHalt("Stop model because external model told us to");
                            break;
                        } else if (command.equals("step")) {
                            if (jsonPayload.has("nbStep") && !jsonPayload.isNull("nbStep")) {
                                int nbStep = jsonPayload.getInt("nbStep");
                                for (int j = 0; j < nbStep; j++) {
                                    sOkStep.release();
                                }
                                nbStepUntilNextCommand = nbStep;
                            }
                        } else if (command.equals("info")) {
                            out.print("current step is "+mi+"\n");
                            sAcceptConnection.release();
                        } else if (command.equals("bilan")) {
                            out.print(getHruBilan());
                            commandIsGet = true;
                            sAcceptConnection.release();
                        } else if (command.equals("get")) {
                            commandIsGet = true;
                            if (jsonPayload.has("key") && !jsonPayload.isNull("key")) {
                                String key = jsonPayload.getString("key");
                                if ("reach".equals(key)) {
                                    out.print(getReachInfoList(null));
                                }
                                else if ("hru".equals(key)) {
                                    out.print(getHruInfoList(null));
                                }
                                else if ("hruDict".equals(key)) {
                                    out.print(getHruInfoDict(null));
                                }
                                else if ("reachDict".equals(key)) {
                                    out.print(getReachInfoDict(null));
                                }
                            }
                            sAcceptConnection.release();
                        } else if (command.equals("getHru")) {
                            commandIsGet = true;
                            if (jsonPayload.has("keys") && !jsonPayload.isNull("keys")) {
                                JSONArray jsonArray = jsonPayload.getJSONArray("keys");
                                List<String> keys = new ArrayList();
                                for (int i = 0; i < jsonArray.length();i++){
                                    keys.add(jsonArray.getString(i));
                                    //log("an attribute name:" + jsonArray.getString(i));
                                };
                                List<Long> ids = new ArrayList();
                                if (jsonPayload.has("ids") && !jsonPayload.isNull("ids")) {
                                    jsonArray = jsonPayload.getJSONArray("ids");
                                    for (int i = 0; i < jsonArray.length();i++){
                                        ids.add(jsonArray.getLong(i));
                                    };
                                }
                                out.print(getAttribAllHruList(keys, ids));
                            }
                            sAcceptConnection.release();
                        } else if (command.equals("getReach")) {
                            commandIsGet = true;
                             if (jsonPayload.has("keys") && !jsonPayload.isNull("keys")) {
                                JSONArray jsonArray = jsonPayload.getJSONArray("keys");
                                 List<String> keys = new ArrayList();
                                 for (int i = 0; i < jsonArray.length();i++){
                                    keys.add(jsonArray.getString(i));
                                    //log("an attribute name:" + jsonArray.getString(i));
                                 };
                                List<Long> ids = new ArrayList();
                                if (jsonPayload.has("ids") && !jsonPayload.isNull("ids")) {
                                    jsonArray = jsonPayload.getJSONArray("ids");
                                    for (int i = 0; i < jsonArray.length();i++){
                                        ids.add(jsonArray.getLong(i));
                                    };
                                }
                                out.print(getAttribAllReachList(keys, ids));
                            }
                            sAcceptConnection.release();
                        } else if (command.equals("getHruSum")) {
                            commandIsGet = true;
                            if (jsonPayload.has("keys") && !jsonPayload.isNull("keys")) {
                                JSONArray jsonArray = jsonPayload.getJSONArray("keys");
                                 List<String> keys = new ArrayList();
                                 for (int i = 0; i < jsonArray.length();i++){
                                 keys.add(jsonArray.getString(i));
                                 //log("an attribute name:" + jsonArray.getString(i));
                                         };
                                out.print(getAttribAllHruSumed(keys));
                            }
                            sAcceptConnection.release();
                        } else if (command.equals("getReachSum")) {
                            commandIsGet = true;
                             if (jsonPayload.has("keys") && !jsonPayload.isNull("keys")) {
                                JSONArray jsonArray = jsonPayload.getJSONArray("keys");
                                 List<String> keys = new ArrayList();
                                 for (int i = 0; i < jsonArray.length();i++){
                                 keys.add(jsonArray.getString(i));
                                 //log("an attribute name:" + jsonArray.getString(i));
                                         };
                                out.print(getAttribAllReachSumed(keys));
                            }
                            sAcceptConnection.release();
                        } else if (command.equals("getCatchmentRunoff")) {
                            commandIsGet = true;
                            out.print(getCatchmentRunoff());
                            sAcceptConnection.release();
                        } else if (command.equals("getHruStorage")) {
                            commandIsGet = true;
                            if (jsonPayload.has("keys") && !jsonPayload.isNull("keys")) {
                                // for further devloppements
                                JSONArray jsonArray = jsonPayload.getJSONArray("keys");
                                 List<String> keys = new ArrayList();
                                 for (int i = 0; i < jsonArray.length();keys.add(jsonArray.getString(i++)));
                                //out.print(getCatchmentHruStorage(keys));
                            } else {
                                out.print(getCatchmentHruStorage());
                            }  
                            sAcceptConnection.release();
                        } else if (command.equals("getReachStorage")) {
                            commandIsGet = true;
                            if (jsonPayload.has("keys") && !jsonPayload.isNull("keys")) {
                                // for further devloppements
                                JSONArray jsonArray = jsonPayload.getJSONArray("keys");
                                 List<String> keys = new ArrayList();
                                 for (int i = 0; i < jsonArray.length();keys.add(jsonArray.getString(i++)));
                                //out.print(getCatchmentReachStorage(keys));
                            } else {
                                out.print(getCatchmentReachStorage());
                            }
                            sAcceptConnection.release();
                        } else if (command.equals("set")) {
                            if (jsonPayload.has("key") && !jsonPayload.isNull("key") &&
                                  jsonPayload.has("value") && !jsonPayload.isNull("value")  ) {
                                String key = jsonPayload.getString("key");
                                if ("i".equals(key)) {
                                    mi = jsonPayload.getInt("value");
                                }
                                // we accept any key (a specific module OR CouplingHruVariableChanger will handle it)
                                else if ("aspersion".equals(key) ||
                                         "drip".equals(key) ||
                                         "surface".equals(key) ||
                                         "infiltration".equals(key) ||
                                         "reachin".equals(key) ||
                                         "reachout".equals(key) ||
                                         true
                                        ) {
                                    JSONObject values = (JSONObject)jsonPayload.get("value");
                                    setTableValues(key, values);
                                }
                            }
                            sAcceptConnection.release();
                        } else {
                            sOkStep.release();
                        }
                    } else {
                        sOkStep.release();
                    }

                    // write JSON response
                    if (!commandIsGet) {
                        out.print("{\n");
                        out.print("  step: " + mi + ",\n");
                        out.print("  payload: \"" + payload + "\"");
                        out.print("\n}\n");
                    }

                    // Close socket, breaking the connection to the client, and
                    // closing the input and output streams
                    out.close(); // Flush and close the output stream
                    in.close(); // Close the input stream
                    client.close(); // Close the socket itself

                    // launch timeout
                    System.err.println("debug mode is: " + debugMode.getValue() + "\n");
                    if (debugMode.getValue() == true)
                    {
                        startTimeout(3600);
                    } else {
                        startTimeout(120);
                    }
                } // Now loop again, waiting for the next connection
            } // If anything goes wrong, print an error message
            catch (Exception e) {
                System.err.println(e);
            }
        }
    }
}
