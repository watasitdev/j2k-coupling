/*
 * IrrigationApplication.java
 * Created on 13.08.2015, 17:42:55
 *
 * This file is part of JAMS
 * Copyright (C) FSU Jena
 *
 * JAMS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * JAMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JAMS. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package cowattools;

import jams.data.*;
import jams.model.*;

//import cowattools.CouplingCommunication;

/**
 *
 * @author Sven Kralisch <sven.kralisch at uni-jena.de>
 */
@JAMSComponentDescription(
        title = "CouplingReachVariableChanger",
        author = "Julien Veyssier",
        description = "Change a variable value for current Reach according to external coupling orders",
        date = "2020-05-13",
        version = "0.1"
)
@VersionComments(entries = {
    @VersionComments.Entry(version = "0.1", comment = "Initial version")
})
public class CouplingReachVariableChanger extends JAMSComponent {

    /*
     *  Component attributes
     */
    
    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READ,
            description = "The reach collection"
    )
    public Attribute.EntityCollection reachs;

    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READ,
            description = "Module name. This is the name you need to use when using j2kSet communication command"
    )
    public Attribute.String moduleName;
    
    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READWRITE,
            description = "Reach attribute you want to change"
    )
    public Attribute.Double attribute;
    
    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READ,
            description = "Do you want to set (0) or add (1) something to the attribute?"
    )
    public Attribute.Long setOrAdd;

    /*
     *  Component run stages
     */
    @Override
    public void run() {
        // get reach ID
        Attribute.Entity reach = reachs.getCurrent();
        Double reachId = reach.getDouble("ID");
        int iReachId = reachId.intValue();
        String sReachId = String.valueOf(iReachId);
        
        // ask communication module what's the value
        String strModuleName = moduleName.getValue();
        //String strAttributeName = attributeName.getValue();
        Long setOrAddValue = setOrAdd.getValue();
        
        Double extValue = CouplingCommunication.getTableValue(strModuleName, String.valueOf(sReachId));
        if (extValue == null) {
            return;
        }

        if (setOrAddValue == 0) {
            attribute.setValue(extValue);
            System.out.println("["+strModuleName+"] [REACH "+sReachId+"] set attribute = "+extValue);
        } else {
            if (extValue > 0.0) {
                Double attributeCurrentValue = attribute.getValue();
                attribute.setValue(attributeCurrentValue + extValue);
                System.out.println("["+strModuleName+"] [REACH "+sReachId+"] add "+extValue+" to attribute");
            }
        }

    }

}
