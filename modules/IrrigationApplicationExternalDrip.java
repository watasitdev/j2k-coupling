/*
 * IrrigationApplication.java
 * Created on 13.08.2015, 17:42:55
 *
 * This file is part of JAMS
 * Copyright (C) FSU Jena
 *
 * JAMS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * JAMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JAMS. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package cowattools;

import jams.data.*;
import jams.model.*;

//import cowattools.CouplingCommunication;

/**
 *
 * @author Sven Kralisch <sven.kralisch at uni-jena.de>
 */
@JAMSComponentDescription(
        title = "IrrigationApplicationExternalDrip",
        author = "Julien Veyssier",
        description = "Apply irrigation on an HRU based on external model",
        date = "2020-02-25",
        version = "0.1"
)
@VersionComments(entries = {
    @VersionComments.Entry(version = "0.1", comment = "Initial version")
})
public class IrrigationApplicationExternalDrip extends JAMSComponent {

    /*
     *  Component attributes
     */
    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READ,
            description = "HRUs list"
    )
    public Attribute.EntityCollection hrus;

    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READWRITE,
            description = "Added water for irrigation",
            unit = "l"
    )
    public Attribute.Double irrigationTotal;

    // for drip irrigation
    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READ,
            description = "maximum MPS",
            unit = "L"
    )
    public Attribute.Double maxMPS;

    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READWRITE,
            description = "state var actual MPS",
            unit = "L"
    )
    public Attribute.Double actMPS;

    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READ,
            description = "HRU area",
            unit = "m²"
    )
    public Attribute.Double area;
    
    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.WRITE,
            description = "What remains after drip irrigation",
            unit = "L"
    )
    public Attribute.Double remainingAfterDrip;

    /*
     *  Component run stages
     */
    @Override
    public void run() {
        // get current HRU ID
        if (hrus == null) {
            System.out.println("HRUSSSS is null");
            return;
        }
        Attribute.Entity hru = hrus.getCurrent();
        Double hruId = hru.getDouble("ID");
        String sHruId = String.valueOf(hruId.intValue());
        Double extDripVal = CouplingCommunication.getTableValue("drip", String.valueOf(sHruId));

        double afterDrip = 0.0;
        remainingAfterDrip.setValue(afterDrip);
        
        if (extDripVal == null) {
            return;
        }

        // CODE OF DRIP
        if (extDripVal != 0.0) {
            // actual irrigation is the minimum of available storage capacity and 
            // availabe irrigation water
            double actDripIrrigation = Math.min(maxMPS.getValue() - actMPS.getValue(), extDripVal);
            // increase actMPS by the irrigation volume
            actMPS.setValue(actMPS.getValue() + actDripIrrigation);
            // decrease irrigationWater by the irrigation volume
            afterDrip = extDripVal - actDripIrrigation;
            // set irrigationTotal to the irrigation volume
            irrigationTotal.setValue(irrigationTotal.getValue() + actDripIrrigation);
        }
        
        remainingAfterDrip.setValue(afterDrip);
    }

}
