/*
 * IrrigationDemand.java
 * Created on 12.08.2015, 16:47:30
 *
 * This file is part of JAMS
 * Copyright (C) FSU Jena
 *
 * JAMS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * JAMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JAMS. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package cowattools;

import jams.data.*;
import jams.model.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import cowattools.CouplingCommunication;

/**
 *
 * @author Julien Veyssier <julien.veyssier at inrae.fr>
 */
@JAMSComponentDescription(
        title = "",
        author = "Julien Veyssier",
        description = "get Irrigation information",
        date = "2020-02-25",
        version = "0.1"
)
@VersionComments(entries = {
    @VersionComments.Entry(version = "0.1", comment = "Initial version")
})
public class IrrigationTest extends JAMSComponent {

    /*
     *  Component attributes
     */
    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READ,
            description = "Reaches list"
    )
    public Attribute.EntityCollection reaches;

    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READ,
            description = "HRUs list"
    )
    public Attribute.EntityCollection hrus;

    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READ,
            description = "Subbasin ID"
    )
    public Attribute.Double subBasin;

    @JAMSVarDescription(
            access = JAMSVarDescription.AccessType.READWRITE,
            description = "What has been irrigated by external model"
    )
    public Attribute.Double irrigationTotal;

    private Map<Long, Attribute.Entity> reachMap = new HashMap();

    /*
     *  Component run stages
     */
    @Override
    public void init() {
        //put all reaches to a map for easier access
        for (Attribute.Entity reach : reaches.getEntities()) {
            reachMap.put(reach.getId(), reach);
        }
    }

    @Override
    public void run() {
        // get current HRU ID
        if (hrus == null) {
            System.out.println("HRUSSSS is null");
            return;
        }
        Attribute.Entity hru = hrus.getCurrent();
        Double hruId = hru.getDouble("ID");
        int iHruId = hruId.intValue();
        String sHruId = String.valueOf(iHruId);

        System.out.println("Irrig total HRUID: " + sHruId
                + " VALUE " + irrigationTotal.getValue()
        );
    }

}
